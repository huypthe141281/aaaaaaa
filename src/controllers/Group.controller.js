const groupService = require("../services/Group.service");
const apiResponse = require("../helper/apiResponse.helper");
class myGroupController {
  async allGroup(req, res) {
    let { _id } = res.locals.user;
    let { _search } = res.locals;
    return apiResponse.successResponseWithData(res, "", {
      _search,
      groups: await groupService.getAllGroup(_id, _search),
    });
  }

  async groupInfor(req, res) {
    let { _id } = res.locals.user;
    let { name } = req.params;
    let { _sort, _page } = res.locals;
    return apiResponse.successResponseWithData(res, "", {
      _sort,
      _page,
      groups: await groupService.getGroupInfor(name, _id, _sort, _page),
    });
  }
  async createGroup(req, res) {
    let { _id } = res.locals.user;
    let { name } = req.body;
    try {
      let rs = await groupService.createGroup(name.trim(), _id);
      if (rs != null) return apiResponse.successResponseWithData(res, "", rs);
      else return apiResponse.badRequestResponse(res, "name already exist", rs);
    } catch (error) {
      apiResponse.serverErrorResponse(res, error);
    }
  }
  async addToGroup(req, res) {
    try {
      let { id } = req.params;
      let { _id } = res.locals.user;
      return apiResponse.successResponseWithData(
        res,
        "",
        await groupService.addToGroup(id, JSON.parse(req.body.addStock), _id)
      );
    } catch (error) {
      apiResponse.serverErrorResponse(res, error.message);
    }
  }
  async removeGroup(req, res) {
    try {
      let { id } = req.body;
      let { _id } = res.locals.user;
      return apiResponse.successResponseWithData(
        res,
        "",
        await groupService.removeGroup(id, _id)
      );
    } catch (error) {
      apiResponse.serverErrorResponse(res, error.message);
    }
  }

  async removeFromGroup(req, res) {
    try {
      let { id } = req.params;
      let { _id } = res.locals.user;
      return apiResponse.successResponseWithData(
        res,
        "",
        await groupService.removeFromGroup(
          id,
          JSON.parse(req.body.removeStock),
          _id
        )
      );
    } catch (error) {
      apiResponse.serverErrorResponse(res, error.message);
    }
  }
}

module.exports = new myGroupController();
