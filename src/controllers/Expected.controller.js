const expectedService = require("../services/Expected.service");
const apiResponse = require("../helper/apiResponse.helper");
class expectedController {
  async getExpectedPrice(req, res) {
    try {
      let { _id } = res.locals.user;
      let { _search, _page, _sort } = res.locals;

      let expected = await expectedService.getExpectedPrice(
        _id,
        _search,
        _page,
        _sort
      );
      return apiResponse.successResponseWithData(res, "Expected price", {
        _sort,
        _page,
        _search,
        expected: expected.length > 0 ? expected[0] : null,
      });
    } catch (error) {
      apiResponse.serverErrorResponse(res, error.message);
    }
  }
  async getExpectedById(req, res) {
    try {
      let { _id } = res.locals.user;
      let { id } = req.params;
      return apiResponse.successResponseWithData(
        res,
        "Expected price",
        await expectedService.getExpectedId(_id, parseInt(id))
      );
    } catch (error) {
      apiResponse.serverErrorResponse(res, error.message);
    }
  }
  async insertExpectedPrice(req, res) {
    try {
      let { _id } = res.locals.user;
      let { stockId, price } = req.body;
      if (isNaN(price)) {
        return apiResponse.badRequestResponse(res, "value not a number");
      }

      return apiResponse.successResponseWithData(
        res,
        "Expected price",
        await expectedService.addExpectedPrice(
          parseInt(_id),
          parseInt(stockId),
          parseInt(price)
        )
      );
    } catch (error) {
      apiResponse.serverErrorResponse(res, error.message);
    }
  }

  async deleteExpectedById(req, res) {
    try {
      let { _id } = res.locals.user;
      let { id } = req.params;
      return apiResponse.successResponseWithData(
        res,
        "Expected price",
        await expectedService.removeExpectedPrice(parseInt(_id), parseInt(id))
      );
    } catch (error) {
      apiResponse.serverErrorResponse(res, error.message);
    }
  }
}

module.exports = new expectedController();
