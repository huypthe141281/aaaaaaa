const stockService = require("../services/Stock.service");
const apiResponse = require("../helper/ApiResponse.helper");

// const asyncRedis = require("async-redis");
// const client = asyncRedis.createClient();
// client.on("connect", () => console.log("redis connect"));
// const set = (key, value) => {
//   client.set(key, JSON.stringify(value));
// };

class StockService {
  async infor(req, res) {
    let { _sort, _page, _search } = res.locals;
    let key = req.originalUrl;
    // let data = await client.get(key);
    // if (!data) {
    //   data = await stockService.getStockInfor(_sort, _page, _search);
    //   set(key, data);
    // }
    return apiResponse.successResponseWithData(res, "", {
      _sort,
      _page,
      _search,
      stocks: await stockService.getStockInfor(_sort, _page, _search),
    });
  }

  async getOneCode(req, res) {
    let { code } = req.params;
    apiResponse.successResponseWithData(
      res,
      "",
      await stockService.getOneCode(code)
    );
  }
}

module.exports = new StockService();
