const validator = require("email-validator");
const _ = require("lodash");

const userService = require("../services/User.service");
const apiResponse = require("../helper/apiResponse.helper");
const jwtHelper = require("../helper/Jwt.helper");
const CONSTANT = require("../helper/Constant.helper");

class UserController {
  async register(req, res, next) {
    try {
      if (!validator.validate(req.body.email)) {
        return apiResponse.badRequestResponse(res, "Email wrong format");
      }

      if (
        req.body.email == undefined ||
        req.body.password == undefined ||
        req.body.name == undefined ||
        req.body.email.trim() == "" ||
        req.body.password.trim() == "" ||
        req.body.name.trim() == ""
      ) {
        return apiResponse.badRequestResponse(res, "Complete all field");
      }

      let user1 = await userService.getUser(req.body.email);

      if (_.isEmpty(user1)) {
        let activeCode = Math.floor(100000 + Math.random() * 900000);
        let user = {
          email: req.body.email,
          password: req.body.password,
          name: req.body.name,
          activeCode,
        };

        await userService.register(user);
        await userService.sendEmail(
          `${CONSTANT.ROOTURL}/user/active?email=${req.body.email}&activeCode=${activeCode}`,
          req.body.email
        );

        return apiResponse.successResponse(
          res,
          "Please check email to active account"
        );
      } else {
        return apiResponse.badRequestResponse(res, "Email already existed");
      }
    } catch (error) {
      return apiResponse.serverErrorResponse(res, error);
    }
  }
  async active(req, res, next) {
    try {
      let user = await userService.activeAccount(
        req.query.email,
        req.query.activeCode
      );

      if (user != undefined && user != null && user != 0)
        return apiResponse.successResponse(res, "active success");
      else {
        return apiResponse.badRequestResponse(res, "active fail");
      }
    } catch (error) {
      return apiResponse.serverErrorResponse(res, error);
    }
  }

  async login(req, res, next) {
    try {
      let email = req.body.email;
      let password = req.body.password;
      if (email.trim() == "" || password.trim() == "") {
        return apiResponse.badRequestResponse(res, "complete all field");
      }
      let user = await userService.getUserLogin(email, password);

      if (!_.isEmpty(user)) {
        jwtHelper
          .signData({ _id: user._id })
          .then((token) => {
            return apiResponse.successResponseWithData(
              res,
              "Login success",
              token
            );
          })
          .catch((error) => {
            return apiResponse.serverErrorResponse(res, error);
          });
      } else {
        return apiResponse.badRequestResponse(res, "Wrong email or password");
      }
    } catch (error) {
      return apiResponse.serverErrorResponse(res, "Server error");
    }
  }
  infor(req, res) {
    return apiResponse.successResponseWithData(
      res,
      "user infor",
      res.locals.user
    );
  }
}

module.exports = new UserController();
