const express = require("express");

const userController = require("../controllers/User.controller");
const loginMiddleware = require("../middlewares/Login.middleware");

const router = express.Router();
//
router.put("/register", userController.register);
router.get("/active", userController.active);
router.post("/login", userController.login);
router.get("/infor", loginMiddleware, userController.infor);
module.exports = router;
