const express = require("express");

const expectedController = require("../controllers/Expected.controller");
const searchMiddleware = require("../middlewares/Search.middleware");
const sortMiddleware = require("../middlewares/Sorting.middleware");
const pageMiddleware = require("../middlewares/Paging.middleware");

const router = express.Router();

router.get(
  "/",
  searchMiddleware,
  sortMiddleware,
  pageMiddleware,
  expectedController.getExpectedPrice
);
router.post("/", expectedController.insertExpectedPrice);
router.get("/:id", expectedController.getExpectedById);
router.delete("/:id", expectedController.deleteExpectedById);

module.exports = router;
