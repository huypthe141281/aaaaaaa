const express = require("express");

const stockController = require("../controllers/Stock.controller");

const router = express.Router();

router.get("/", stockController.infor);
//router.get("/:code", stockController.inforByCode);
router.get("/:code", stockController.getOneCode);
module.exports = router;
