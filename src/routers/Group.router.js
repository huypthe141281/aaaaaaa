const express = require("express");

const groupController = require("../controllers/Group.controller");
const sortMiddleware = require("../middlewares/Sorting.middleware");
const pageMiddleware = require("../middlewares/Paging.middleware");
const searchMiddleware = require("../middlewares/Search.middleware");

const router = express.Router();
router.get("/", searchMiddleware, groupController.allGroup);
router.post("/", groupController.createGroup);

router.get(
  "/:name",
  sortMiddleware,
  pageMiddleware,
  groupController.groupInfor
);
router.post("/:id", groupController.addToGroup);
router.delete("/", groupController.removeGroup);
router.delete("/:id", groupController.removeFromGroup);
module.exports = router;
