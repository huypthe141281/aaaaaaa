process.env.NODE_ENV = "test";

const chai = require("chai");
const chaiHttp = require("chai-http");
const _ = require("lodash");

const server = require("../../server");
const expected = require("../models/ExpectedPrice");
const user = require("../models/User");
const currentId = require("../models/CurrentId");
const stockService = require("../services/Stock.service");

const should = chai.should();

chai.use(chaiHttp);

describe("Test expected prices", function () {
  this.timeout(10000);
  beforeEach(async (done) => {
    await currentId.collection.insertMany([
      { _id: "expectedPrice", count: 3 },
      { _id: "stock", count: 0 },
    ]);
    await stockService.autoUpdate();
    await user.insertMany([
      {
        _id: 1,
        active: true,
        activeCode: "243416",
        email: "a@b.c",
        password: "123",
        name: "huy",
      },
      {
        _id: 2,
        active: false,
        activeCode: "123456",
        email: "a@b.c1",
        password: "123",
        name: "huy",
      },
    ]);
    await expected.insertMany([
      {
        _id: 1,
        user: 1,
        expected: [
          {
            stock: 2,
            value: 9,
          },
          {
            stock: 3,
            value: 30,
          },
          {
            stock: 9,
            value: 15,
          },
          {
            stock: 10,
            value: 11,
          },
          {
            stock: 1,
            value: 19,
          },
        ],
      },
      {
        _id: 2,
        user: 2,
        expected: [
          {
            stock: 1,
            value: 14,
          },
          {
            stock: 9,
            value: 17,
          },
        ],
      },
    ]);
    done();
  });
  afterEach(async (done) => {
    await stockService.getStockModel().collection.deleteMany({});
    await user.collection.deleteMany({});
    await expected.collection.deleteMany({});
    await currentId.collection.deleteMany({});
    done();
  });
  it("Get expected without login", (done) => {
    chai
      .request(server)
      .get("/expected")
      .end((err, res) => {
        res.should.have.status(401);
        res.body.message.should.eq("Must login");
        done();
      });
  });
  it("Get expected", (done) => {
    chai
      .request(server)
      .get("/expected")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);
        let { data } = res.body;
        if (data.expected != null) {
          data.expected.user._id.should.eq(1);
          data.expected.totalItem.should.eq(data.expected.stocks.length);
        }
        done();
      });
  });
  it("Get expected price sort value", (done) => {
    chai
      .request(server)
      .get("/expected?_sort&column=value&type=1")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);
        let { data } = res.body;
        if (data.expected != null) {
          data.expected.user._id.should.eq(1);
          data.expected.totalItem.should.eq(data.expected.stocks.length);
          let expecteds = data.expected.stocks;
          for (let i = 0; i < expecteds.length - 1; i++) {
            if (expecteds[i].value > expecteds[i + 1].value) {
              throw new Error("Not sorted by value");
            }
          }
        }
        done();
      });
  });
  it("Sort by another fields", (done) => {
    chai
      .request(server)
      .get("/expected?_sort&column=tc&type=1")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);
        let { data } = res.body;
        if (data.expected != null) {
          data.expected.totalItem.should.eq(data.expected.stocks.length);
          data.expected.user._id.should.eq(1);
          let expecteds = data.expected.stocks;
          for (let i = 0; i < expecteds.length - 1; i++) {
            if (expecteds[i].stock.tc > expecteds[i + 1].stock.tc) {
              throw new Error("Not sorted by tc");
            }
          }
        }
        done();
      });
  });
  it("Search expected price", (done) => {
    chai
      .request(server)
      .get("/expected?_search&searchby=code&infor=AA")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);
        let { data } = res.body;
        if (data.expected != null) {
          data.expected.totalItem.should.eq(data.expected.stocks.length);
          data.expected.user._id.should.eq(1);
          let expecteds = data.expected.stocks;
          for (let expected of expecteds) {
            if (!expected.stock.code.includes("AA")) {
              throw new Error("Not search by AA");
            }
          }
        }
        done();
      });
  });
  it("Page expected price", (done) => {
    chai
      .request(server)
      .get("/expected?_page&page=2&size=2")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);
        let { data } = res.body;
        if (data.expected != null) {
          data.expected.totalItem.should.eq(5);
          data.expected.stocks.length.should.eq(2);
          data.expected.user._id.should.eq(1);
        }
        done();
      });
  });
  it("Expected price all", (done) => {
    chai
      .request(server)
      .get(
        "/expected?_page&page=1&size=2&_search&searchby=code&infor=AA&_sort&column=tc&type=1"
      )
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);
        let { data } = res.body;
        if (data.expected != null) {
          data.expected.totalItem.should.eq(3);
          data.expected.stocks.length.should.eq(2);
          data.expected.user._id.should.eq(1);
          let expecteds = data.expected.stocks;
          for (let i = 0; i < expecteds.length - 1; i++) {
            if (expecteds[i].stock.tc > expecteds[i + 1].stock.tc) {
              throw new Error("Not sorted by tc");
            }
          }
        }
        done();
      });
  });
  it("Insert expected price", (done) => {
    chai
      .request(server)
      .post("/expected")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ price: 17, stockId: 4 })
      .end(async (err, res) => {
        let { data } = res.body;
        data.result.nModified.should.eq(1);
        let exp = await expected.find({ user: 1 });
        if (
          !exp[0].expected.some((ex) => {
            return ex.value == 17 && ex.stock == 4;
          })
        ) {
          throw new Error("add fail");
        }
        res.should.have.status(200);
        done();
      });
  });

  it("Update expected price", (done) => {
    chai
      .request(server)
      .post("/expected")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ price: 8, stockId: 2 })
      .end(async (err, res) => {
        let { data } = res.body;
        data.result.nModified.should.eq(1);
        let exp = await expected.find({ user: 1 });
        if (
          !exp[0].expected.some((ex) => {
            return ex.value == 8 && ex.stock == 2;
          })
        ) {
          throw new Error("add fail");
        }
        if (
          exp[0].expected.filter((ex) => {
            return ex.stock == 2;
          }).length != 1
        ) {
          throw new Error("update fail");
        }
        res.should.have.status(200);
        done();
      });
  });
});
