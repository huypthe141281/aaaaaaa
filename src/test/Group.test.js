process.env.NODE_ENV = "test";

const chai = require("chai");
const chaiHttp = require("chai-http");

const server = require("../../server");
const group = require("../models/Group");
const user = require("../models/User");
const currentId = require("../models/CurrentId");
const stockService = require("../services/Stock.service");

const should = chai.should();
chai.use(chaiHttp);

describe("Test group", function () {
  this.timeout(10000);
  beforeEach(async (done) => {
    await currentId.collection.insertMany([
      { _id: "group", count: 3 },
      { _id: "stock", count: 0 },
    ]);
    await stockService.autoUpdate();
    await user.insertMany([
      {
        _id: 1,
        active: true,
        activeCode: "243416",
        email: "a@b.c",
        password: "123",
        name: "huy",
      },
      {
        _id: 2,
        active: false,
        activeCode: "123456",
        email: "a@b.c1",
        password: "123",
        name: "huy",
      },
    ]);
    await group.insertMany([
      {
        _id: 1,
        listStock: [1, 2, 3],
        name: "group2",
        user: 2,
      },
      {
        _id: 2,
        listStock: [9, 10, 15, 2, 3],
        name: "hic",
        user: 1,
      },
      {
        _id: 3,
        listStock: [1, 4, 15, 2, 9],
        name: "kiki",
        user: 1,
      },
    ]);

    done();
  });
  afterEach(async (done) => {
    await stockService.getStockModel().collection.deleteMany({});
    await user.collection.deleteMany({});
    await group.collection.deleteMany({});
    await currentId.collection.deleteMany({});
    done();
  });
  it("all group", (done) => {
    chai
      .request(server)
      .get("/group")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.groups.length.should.gt(0);
        done();
      });
  });
  it("all group fail token", (done) => {
    chai
      .request(server)
      .get("/group")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI231"
      )
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });

  it("search group", (done) => {
    chai
      .request(server)
      .get("/group?_search&searchby=name&infor=i")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.groups.length.should.gt(0);
        for (let group of res.body.data.groups) {
          if (!group.name.includes("i")) {
            throw new Error("search group fail");
          }
        }
        done();
      });
  });
  it("Create group", (done) => {
    chai
      .request(server)
      .post("/group")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ name: "ahuhu" })
      .end(async (err, res) => {
        res.should.have.status(200);
        let group1 = await group.findOne({ name: "ahuhu", user: 1 });
        if (group1 == null) {
          throw new Error("Create fail");
        }
        done();
      });
  });

  it("Create already group", (done) => {
    chai
      .request(server)
      .post("/group")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ name: "hic" })
      .end(async (err, res) => {
        res.should.have.status(400);
        res.body.message.should.eq("name already exist");
        let group1 = await group.findOne({ name: "ahuhu", user: 1 });
        if (group1 != null) {
          throw new Error("Add already");
        }
        done();
      });
  });
  it("Get group by name", (done) => {
    chai
      .request(server)
      .get("/group/hic")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);

        res.body.data.groups.totalItem.should.eq(
          res.body.data.groups.stocks.length
        );
        res.body.data.groups.user._id.should.eq(1);
        done();
      });
  });

  it("Get group by name sort by code", (done) => {
    chai
      .request(server)
      .get("/group/hic?_sort&column=tc&type=1")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);

        let groups = res.body.data.groups;
        groups.totalItem.should.eq(groups.stocks.length);
        groups.user._id.should.eq(1);
        for (let i = 0; i < groups.length - 1; i++) {
          if (groups[i].tc > groups[i].tc) {
            throw new Error("No sort group");
          }
        }
        done();
      });
  });

  it("Get group paging", (done) => {
    chai
      .request(server)
      .get("/group/hic?_sort&column=tc&type=1&_page&page=1&size=2")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .end((err, res) => {
        res.should.have.status(200);

        let groups = res.body.data.groups;

        groups.stocks.length.should.eq(2);
        groups.user._id.should.eq(1);
        for (let i = 0; i < groups.stocks.length - 1; i++) {
          if (groups.stocks[i].tc > groups.stocks[i].tc) {
            throw new Error("No sort group");
          }
        }
        done();
      });
  });
  it("Test add To Group", (done) => {
    chai
      .request(server)
      .post("/group/2")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ addStock: JSON.stringify([1, 2, 4]) })
      .end(async (err, res) => {
        res.should.have.status(200);
        res.body.data.nModified.should.eq(1);
        let stocks = await group.findOne({ user: 1 }).populate("listStock");
        if (stocks.listStock.filter((stock) => stock._id == 2).length != 1) {
          throw new Error("duplicate key");
        }
        if (stocks.listStock.filter((stock) => stock._id == 1).length != 1) {
          throw new Error("duplicate key");
        }
        if (stocks.listStock.filter((stock) => stock._id == 4).length != 1) {
          throw new Error("duplicate key");
        }
        done();
      });
  });
  it("Test add To Group trung", (done) => {
    chai
      .request(server)
      .post("/group/2")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ addStock: JSON.stringify([2, 3]) })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.nModified.should.eq(0);
        done();
      });
  });
  it("Remove groups", (done) => {
    chai
      .request(server)
      .delete("/group")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ id: 2 })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.deletedCount.should.eq(1);
        done();
      });
  });
  it("Remove groups fail", (done) => {
    chai
      .request(server)
      .delete("/group")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ id: 1 })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.deletedCount.should.eq(0);
        done();
      });
  });
  it("Remove from group", (done) => {
    chai
      .request(server)
      .delete("/group/2")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ removeStock: JSON.stringify([1, 2, 3]) })
      .end(async (err, res) => {
        let stocks = await group.findOne({ user: 1 }).populate("listStock");

        if (stocks.listStock.filter((stock) => stock._id == 2).length != 0) {
          throw new Error("Cannot delete");
        }
        if (stocks.listStock.filter((stock) => stock._id == 1).length != 0) {
          throw new Error("Cannot delete");
        }
        if (stocks.listStock.filter((stock) => stock._id == 3).length != 0) {
          throw new Error("Cannot delete");
        }
        res.should.have.status(200);
        done();
      });
  });
  it("Remove from group", (done) => {
    chai
      .request(server)
      .delete("/group/2")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ removeStock: JSON.stringify([1]) })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.nModified.should.eq(0);
        done();
      });
  });

  it("Remove another group", (done) => {
    chai
      .request(server)
      .delete("/group/1")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDI2MDY3N30.XBj3u66i-zR6RSqZpPVbjb-uFyKOQYfHSbbJdUmg8GI"
      )
      .send({ removeStock: JSON.stringify([1, 2]) })
      .end((err, res) => {
        res.should.have.status(500);
        done();
      });
  });
});
