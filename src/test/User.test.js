process.env.NODE_ENV = "test";

const chai = require("chai");
const chaiHttp = require("chai-http");

const server = require("../../server");
const user = require("../models/User");
const currentId = require("../models/CurrentId");

const should = chai.should();
chai.use(chaiHttp);

describe("Test User", function () {
  this.timeout(10000);
  beforeEach(async (done) => {
    await currentId.collection.insertOne({ _id: "user", count: 2 });
    await user.collection.insertMany([
      {
        _id: 1,
        active: true,
        activeCode: "243416",
        email: "a@b.c",
        password: "123",
        name: "huy",
      },
      {
        _id: 2,
        active: false,
        activeCode: "123456",
        email: "a@b.c1",
        password: "123",
        name: "huy",
      },
    ]);

    done();
  });
  afterEach(async (done) => {
    await user.collection.deleteMany({});
    await currentId.deleteMany({});
    done();
  });
  it("Test get information", (done) => {
    chai
      .request(server)
      .get("/user/infor")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsImlhdCI6MTYyNDA2Njc3OH0.ubyQ9TLNPRC70Fcx_RdvJWJeFVK5FbT2PcSln1NIZso"
      )
      .send()
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.email.should.eq("a@b.c");
        done();
      });
  });
  it("Test login sussess", (done) => {
    chai
      .request(server)
      .post("/user/login")
      .send({ email: "a@b.c", password: "123" })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.status.should.eq(1);
        res.body.data.should.not.eq(null);
        done();
      });
  });

  it("Test login active fail", (done) => {
    chai
      .request(server)
      .post("/user/login")
      .send({ email: "a@b.c1", password: "123" })
      .end((err, res) => {
        res.should.have.status(400);
        res.body.status.should.eq(0);
        done();
      });
  });

  it("Test login fail", (done) => {
    chai
      .request(server)
      .post("/user/login")
      .send({ email: "adsadsbdsa.sadc", password: "123" })
      .end((err, res) => {
        res.should.have.status(400);
        res.body.status.should.eq(0);
        done();
      });
  });
  it("Test register", (done) => {
    chai
      .request(server)
      .put("/user/register")
      .send({ email: "huy@gmail.com", password: "123", name: "huyne" })
      .end(async (err, res) => {
        res.should.have.status(200);
        let user1 = await user.findOne({
          email: "huy@gmail.com",
          password: "123",
        });
        if (user1 == null) {
          throw new Error("User not insert to db");
        }
        done();
      });
  });
  it("Test register fail", (done) => {
    chai
      .request(server)
      .put("/user/register")
      .send({ email: "sss", password: "123", name: "huyne" })
      .end(async (err, res) => {
        res.should.have.status(400);
        let user1 = await user.findOne({
          email: "sss",
          password: "123",
        });
        if (user1 != null) {
          throw new Error("User insert to db");
        }
        done();
      });
  });
  it("Test register trung", (done) => {
    chai
      .request(server)
      .put("/user/register")
      .send({ email: "a@b.c", password: "123", name: "huyne" })
      .end(async (err, res) => {
        res.should.have.status(400);
        let user1 = await user.find({
          email: "a@b.c",
          password: "123",
        });
        if (user1.length > 1) {
          throw new Error("User insert to db");
        }
        done();
      });
  });
  it("Test active account", (done) => {
    chai
      .request(server)
      .get("/user/active?email=a@b.c1&activeCode=123456")
      .end(async (err, res) => {
        res.should.have.status(200);
        let user1 = await user.findOne({
          email: "a@b.c1",
          password: "123",
          active: true,
        });
        if (user1 == null) {
          throw new Error("Can not active");
        }
        done();
      });
  });
});
