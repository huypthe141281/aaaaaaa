process.env.NODE_ENV = "test";

const chai = require("chai");
const chaiHttp = require("chai-http");

const server = require("../../server");
const stockService = require("../services/Stock.service");
const currentId = require("../models/CurrentId");

const should = chai.should();
chai.use(chaiHttp);

describe("Test Stock", function () {
  this.timeout(10000);
  beforeEach(async (done) => {
    await currentId.collection.insertOne({ _id: "stock", count: 0 });
    await stockService.autoUpdate();
    done();
  });

  afterEach(async (done) => {
    await stockService.getStockModel().deleteMany({});
    await currentId.deleteMany({});
    done();
  });

  it("Test get all stock from database", (done) => {
    chai
      .request(server)
      .get("/stock")
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.stocks.stocks.length.should.gt(0);
        res.body.data.stocks.stocks.length.should.eq(
          res.body.data.stocks.totalItem
        );
        done();
      });
  });

  if (
    ("test search from database",
    (done) => {
      chai
        .request(server)
        .get("/stock?_search&searchby=code&infor=aa")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.stocks.stocks.length.should.gt(0);
          res.body.data.stocks.stocks.length.should.eq(
            res.body.data.stocks.totalItem
          );
          for (let stock of res.body.data.stocks.stocks) {
            if (!stock.code.includes("AA")) {
              throw new Error("search fail");
            }
          }
          done();
        });
    })
  )
    it("Test sort asc code", (done) => {
      chai
        .request(server)
        .get("/stock?_sort&column=code&type=1")
        .end((err, res) => {
          res.should.have.status(200);
          let stocks = res.body.data.stocks.stocks;
          stocks.length.should.gt(0);
          stocks.length.should.eq(res.body.data.stocks.totalItem);
          for (let i = 0; i < stocks.length - 1; i++) {
            if (stocks[i].code > stocks[i + 1].code) {
              throw new Error("Not sorted by code");
            }
          }
          done();
        });
    });

  it("Test sort asc tc", (done) => {
    chai
      .request(server)
      .get("/stock?_sort&column=tc&type=1")
      .end((err, res) => {
        res.should.have.status(200);
        let stocks = res.body.data.stocks.stocks;
        stocks.length.should.gt(0);
        stocks.length.should.eq(res.body.data.stocks.totalItem);
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].tc > stocks[i + 1].tc) {
            throw new Error("Not sorted by tc");
          }
        }
        done();
      });
  });

  it("Test sort des tc", (done) => {
    chai
      .request(server)
      .get("/stock?_sort&column=tc&type=-1")
      .end((err, res) => {
        res.should.have.status(200);
        let stocks = res.body.data.stocks.stocks;
        stocks.length.should.gt(0);
        stocks.length.should.eq(res.body.data.stocks.totalItem);
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].tc < stocks[i + 1].tc) {
            throw new Error("Not sorted by tc");
          }
        }
        done();
      });
  });

  it("Test sort wrong type", (done) => {
    chai
      .request(server)
      .get("/stock?_sort&column=tc&type=dsa")
      .end((err, res) => {
        res.should.have.status(400);
        res.body.status.should.eq(0);
        res.body.message.should.eq("bad params value ");
        done();
      });
  });

  it("Test sort and paging", async (done) => {
    let size = await stockService.getStockModel().count();
    chai
      .request(server)
      .get("/stock?_sort&column=tc&type=1&_page&page=1&size=20")
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.stocks.stocks.length.should.eq(Math.min(size, 20));
        res.body.data.stocks.totalItem.should.eq(size);
        let stocks = res.body.data.stocks.stocks;
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].tc > stocks[i + 1].tc) {
            throw new Error("Not sorted by tc");
          }
        }
        done();
      });
  });

  it("Test sort and paging big page", async (done) => {
    let total = await stockService.getStockModel().count();
    let page = total / 20 + 1;
    chai
      .request(server)
      .get(`/stock?_sort&column=tc&type=1&_page&page=${page}&size=20`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.stocks.stocks.length.should.eq(0);
        res.body.data.stocks.totalItem.should.eq(total);
        let stocks = res.body.data.stocks.stocks;
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].tc > stocks[i + 1].tc) {
            throw new Error("Not sorted by tc");
          }
        }
        done();
      });
  });

  it("Test paging wrong input", (done) => {
    chai
      .request(server)
      .get("/stock?_sort&column=tc&type=1&_page&page=1&size=a")
      .end((err, res) => {
        res.should.have.status(400);
        res.body.status.should.eq(0);
        done();
      });
  });

  it("Test sort asc code", (done) => {
    chai
      .request(server)
      .get("/stock?_search&searchby=code&infor=a&_sort&column=code&type=1")
      .end((err, res) => {
        res.should.have.status(200);
        let stocks = res.body.data.stocks.stocks;
        stocks.length.should.gt(0);
        stocks.length.should.eq(res.body.data.stocks.totalItem);
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].code > stocks[i + 1].code) {
            throw new Error("Not sorted by code");
          }
        }
        done();
      });
  });

  it("Test sort asc tc", (done) => {
    chai
      .request(server)
      .get("/stock?_search&searchby=code&infor=a&_sort&column=tc&type=1")
      .end((err, res) => {
        res.should.have.status(200);
        let stocks = res.body.data.stocks.stocks;
        stocks.length.should.gt(0);
        stocks.length.should.eq(res.body.data.stocks.totalItem);
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].tc > stocks[i + 1].tc) {
            throw new Error("Not sorted by tc");
          }
        }
        done();
      });
  });

  it("Test sort des tc", (done) => {
    chai
      .request(server)
      .get("/stock?_search&searchby=code&infor=a&_sort&column=tc&type=-1")
      .end((err, res) => {
        res.should.have.status(200);
        let stocks = res.body.data.stocks.stocks;
        stocks.length.should.gt(0);
        stocks.length.should.eq(res.body.data.stocks.totalItem);
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].tc < stocks[i + 1].tc) {
            throw new Error("Not sorted by tc");
          }
        }
        done();
      });
  });

  it("Test sort wrong type", (done) => {
    chai
      .request(server)
      .get("/stock?_search&searchby=code&infor=a&_sort&column=tc&type=dsa")
      .end((err, res) => {
        res.should.have.status(400);
        res.body.status.should.eq(0);
        res.body.message.should.eq("bad params value ");
        done();
      });
  });

  it("Test sort and paging", async (done) => {
    let size = await stockService
      .getStockModel()
      .find({ code: { $regex: "A" } })
      .count();
    chai
      .request(server)
      .get(
        "/stock?_search&searchby=code&infor=a&_sort&column=tc&type=1&_page&page=1&size=20"
      )
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.stocks.stocks.length.should.eq(Math.min(size, 20));
        res.body.data.stocks.totalItem.should.eq(size);
        let stocks = res.body.data.stocks.stocks;
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].tc > stocks[i + 1].tc) {
            throw new Error("Not sorted by tc");
          }
        }
        done();
      });
  });

  it("Test sort and paging big page", async (done) => {
    let total = await stockService
      .getStockModel()
      .find({ code: { $regex: "A" } })
      .count();
    let page = total / 20 + 1;
    chai
      .request(server)
      .get(
        `/stock?_search&searchby=code&infor=a&_sort&column=tc&type=1&_page&page=${page}&size=20`
      )
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.stocks.stocks.length.should.eq(0);
        res.body.data.stocks.totalItem.should.eq(total);
        let stocks = res.body.data.stocks.stocks;
        for (let i = 0; i < stocks.length - 1; i++) {
          if (stocks[i].tc > stocks[i + 1].tc) {
            throw new Error("Not sorted by tc");
          }
        }
        done();
      });
  });

  it("Test paging wrong input", (done) => {
    chai
      .request(server)
      .get(
        "/stock?_search&searchby=code&infor=a&_sort&column=tc&type=1&_page&page=1&size=a"
      )
      .end((err, res) => {
        res.should.have.status(400);
        res.body.status.should.eq(0);
        done();
      });
  });
});

describe("Test Stock Infor", function () {
  this.timeout(10000);
  beforeEach(async (done) => {
    await currentId.collection.insertOne({ _id: "stock", count: 0 });
    await stockService.autoUpdate();
    done();
  });

  afterEach(async (done) => {
    await stockService.getStockModel().deleteMany({});
    await currentId.deleteMany({});
    done();
  });
  it("Test get all stock have code contain a from database", (done) => {
    chai
      .request(server)
      .get("/stock/AAA")
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.code.should.eq("AAA");

        done();
      });
  });
});
