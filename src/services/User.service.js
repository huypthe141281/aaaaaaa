const userModel = require("../models/User");
const mailer = require("../helper/Mailler.helper");
const currentId = require("../helper/CurrentId.helper");

class UserService {
  async register(user) {
    try {
      let id = await currentId.getId("user");
      return await userModel.insertMany([Object.assign({ _id: id + 1 }, user)]);
    } catch (error) {
      throw error;
    }
  }
  async getUser(email) {
    try {
      return await userModel.findOne({ email });
    } catch (error) {
      throw error;
    }
  }

  async sendEmail(content, email) {
    mailer.send(email, "Hello ✔", "Please check email to register", content);
  }

  async activeAccount(email, activeCode) {
    try {
      return await userModel.updateOne({ email, activeCode }, { active: true });
    } catch (error) {
      throw error;
    }
  }

  async getUserLogin(email, password) {
    try {
      return await userModel.findOne({ email, password, active: true });
    } catch (error) {
      throw error;
    }
  }

  async getUserById(_id) {
    try {
      return await userModel.findOne({ _id });
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new UserService();
