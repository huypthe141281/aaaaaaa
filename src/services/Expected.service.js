const mongoose = require("mongoose");
const _ = require("lodash");

const expectedPrice = require("../models/ExpectedPrice");
const currentId = require("../helper/CurrentId.helper");
class expectedService {
  getExpectedPrice(userId, _search, _page, _sort) {
    let searchObject = {};
    if (_search.enabled == true) {
      searchObject = {
        ["stocks." + _search.searchby]: {
          $regex: "" + _search.infor.trim().toUpperCase() + "",
        },
      };
    }
    let sortObject = { "stocks._id": 1 };
    if (_sort.enabled == true) {
      if (_sort.column != "value") {
        sortObject = {
          ["stocks." + _sort.column]: parseInt(_sort.type),
        };
      } else {
        sortObject = {
          ["expected.value"]: parseInt(_sort.type),
        };
      }
    }
    let [page, size] = [1, 10];
    if (_page.enabled == true) {
      page = parseInt(_page.page);
      size = parseInt(_page.size);
    }
    return expectedPrice.aggregate([
      { $match: { user: userId } },
      { $unwind: "$expected" },
      {
        $lookup: {
          from: "stocks",
          localField: "expected.stock",
          foreignField: "_id",
          as: "stocks",
        },
      },
      { $unwind: "$stocks" },
      { $match: searchObject },
      {
        $facet: {
          user: [
            {
              $lookup: {
                from: "users",
                let: { id: "$_id" },
                pipeline: [
                  { $match: { $expr: { $eq: ["$$id", "$_id"] } } },
                  { $project: { email: 1, name: 1 } },
                ],
                as: "user",
              },
            },
            { $group: { _id: "$_id", user: { $first: "$user" } } },
            { $unwind: "$user" },
          ],
          totalItem: [{ $count: "id" }],
          stocks: [
            { $sort: sortObject },
            { $skip: (page - 1) * size },
            { $limit: size },
            { $unwind: "$user" },
            {
              $group: {
                _id: "$_id",

                expected: {
                  $push: { stock: "$stocks", value: "$expected.value" },
                },
              },
            },
          ],
        },
      },
      {
        $set: {
          user: "$user.user",
          totalItem: "$totalItem.id",
          stocks: "$stocks.expected",
        },
      },
      { $unwind: "$user" },
      { $unwind: "$totalItem" },
      { $unwind: "$stocks" },
    ]);
  }

  getExpectedId(userId, stockId) {
    return expectedPrice.aggregate([
      { $match: { user: userId } },
      { $unwind: "$expected" },
      { $match: { "expected.stock": stockId } },
      {
        $lookup: {
          from: "stocks",
          localField: "expected.stock",
          foreignField: "_id",
          as: "stocks",
        },
      },
      { $unwind: "$stocks" },
      {
        $group: {
          _id: "$_id",
          expected: { $push: { stock: "$stocks", value: "$expected.value" } },
        },
      },
    ]);
  }

  async addExpectedPrice(userId, stockId, price) {
    if (
      _.isEmpty(
        await this.getExpectedPrice(
          userId,
          { enabled: false },
          { enabled: false },
          { enabled: false }
        )
      )
    ) {
      return await expectedPrice.collection.insertMany([
        {
          _id: (await currentId.getId("expectedPrice")) + 1,
          user: userId,
          expected: [{ stock: stockId, value: price }],
        },
      ]);
    } else if (_.isEmpty(await this.getExpectedId(userId, stockId))) {
      return await expectedPrice.collection.updateOne(
        { user: userId },
        { $push: { expected: { stock: stockId, value: price } } }
      );
    } else {
      return await expectedPrice.collection.updateOne(
        {
          user: userId,
          "expected.stock": stockId,
        },
        {
          $set: {
            "expected.$.value": price,
          },
        }
      );
    }
  }
  removeExpectedPrice(userId, stockId) {
    return expectedPrice.collection.updateOne(
      {
        user: userId,
      },
      {
        $pull: { expected: { stock: stockId } },
      }
    );
  }
}

module.exports = new expectedService();
