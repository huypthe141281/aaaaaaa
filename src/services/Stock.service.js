const axios = require("axios");
const _ = require("lodash");

const stockModel = require("../models/stock");
const currentId = require("../helper/CurrentId.helper");

class StockService {
  constructor() {
    this.url = "https://banggia.cafef.vn/stockhandler.ashx?center=undefined";
  }
  getStockModel() {
    return stockModel;
  }
  async getStockInfor(_sort, _page, _search) {
    let searchObject = {};
    if (_search.enabled == true)
      searchObject = {
        [_search.searchby]: {
          $regex: "" + _search.infor.trim().toUpperCase() + "",
        },
      };

    return {
      totalItem: await stockModel.find(searchObject).countDocuments(),
      stocks: await stockModel
        .find(searchObject)
        .pagingable(_page)
        .sortable(_sort),
    };
  }

  getStock(stock) {
    for (let z in stock) {
      if (
        z == "a" ||
        z == "x" ||
        z == "y" ||
        z == "z" ||
        z == "Time" ||
        z == "tb" ||
        z == "ts"
      ) {
        continue;
      }
      if (isNaN(stock[z])) {
        return null;
      }
    }
    return {
      code: stock.a,
      tc: stock.b,
      ceil: stock.c,
      floor: stock.d,
      buyPrice3: stock.e,
      buyKl3: stock.f,
      buyPrice2: stock.g,
      buyKl2: stock.h,
      buyPrice1: stock.i,
      buyKl1: stock.j,
      delta: stock.k,
      price: stock.l,
      kl: stock.m,
      tongkl: stock.n,
      salePrice1: stock.o,
      saleKl1: stock.p,
      salePrice2: stock.q,
      saleKl2: stock.r,
      salePrice3: stock.s,
      saleKl3: stock.t,
      high: stock.v,
      low: stock.w,
    };
  }

  getStock2(stock) {
    return {
      code: stock.code,
      tc: stock.tc,
      ceil: stock.ceil,
      floor: stock.floor,
      buyPrice3: stock.buyPrice3,
      buyKl3: stock.buyKl3,
      buyPrice2: stock.buyPrice2,
      buyKl2: stock.buyKl2,
      buyPrice1: stock.buyPrice1,
      buyKl1: stock.buyKl1,
      delta: stock.delta,
      price: stock.price,
      kl: stock.kl,
      tongkl: stock.tongkl,
      salePrice1: stock.salePrice1,
      saleKl1: stock.saleKl1,
      salePrice2: stock.salePrice2,
      saleKl2: stock.saleKl2,
      salePrice3: stock.salePrice3,
      saleKl3: stock.saleKl3,
      high: stock.high,
      low: stock.low,
    };
  }
  async update(stock) {
    try {
      return await stockModel.updateOne({ code: stock.code }, stock);
    } catch (error) {
      throw error;
    }
  }
  async getByCode(code) {
    try {
      return await stockModel.findOne({ code: code });
    } catch (error) {
      throw error;
    }
  }

  async autoUpdate() {
    const url1 = this.url;

    return new Promise((resolve, reject) => {
      axios
        .get(url1)
        .then(async (response) => {
          let insert = [];
          let update = [];
          let all = [];
          for (let stock of response.data) {
            let stock1 = await this.getByCode(stock.a);
            if (this.getStock(stock) == null) {
              continue;
            }

            all.push(this.getStock(stock));
            if (
              !_.isEmpty(stock1) &&
              JSON.stringify(this.getStock2(stock1)) !=
                JSON.stringify(this.getStock(stock))
            ) {
              await this.update(this.getStock(stock));
              update.push(this.getStock(stock));
            }
            if (_.isEmpty(stock1)) {
              insert.push(
                Object.assign(
                  { _id: (await currentId.getId("stock")) + 1 },
                  this.getStock(stock)
                )
              );
            }
          }
          await stockModel.insertMany(insert);
          resolve({ insert, update, all });
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
  getOneCode(code) {
    return stockModel.findOne({ code });
  }
}

module.exports = new StockService();
