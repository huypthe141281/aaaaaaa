const groupModel = require("../models/Group");
const CurrentId = require("../helper/CurrentId.helper");
class myGroupService {
  async getAllGroup(_id, _search) {
    let searchObject = {};
    if (_search.enabled == true)
      searchObject = {
        [_search.searchby]: {
          $regex: "" + _search.infor.trim() + "",
        },
      };

    let groups = await groupModel
      .find(Object.assign({ user: _id }, searchObject))
      .populate("listStock");
    return groups;
  }
  async getGroupInfor(name, _id, _sort, _page) {
    let size = await groupModel.aggregate([
      { $match: { name, user: _id } },
      { $project: { size: { $size: "$listStock" } } },
    ]);
    let group = groupModel.aggregate([
      { $match: { name, user: _id } },
      { $project: { listStock: 1 } },
      { $unwind: "$listStock" },
      {
        $lookup: {
          from: "stocks",
          localField: "listStock",
          foreignField: "_id",
          as: "stocks",
        },
      },
      {
        $unwind: "$stocks",
      },
      {
        $group: {
          _id,
          listStock: { $push: "$stocks" },
        },
      },
      {
        $lookup: {
          from: "users",
          let: { id: "$_id" },
          pipeline: [
            { $match: { $expr: { $eq: ["$$id", "$_id"] } } },
            { $project: { email: 1, name: 1 } },
          ],
          as: "user",
        },
      },
    ]);
    let group1 = await this.pageGroup(this.sortGroup(group, _sort), _page);

    return {
      totalItem: size.length > 0 ? size[0].size : -1,

      user:
        group1.length > 0
          ? group1[0].user != undefined
            ? group1[0].user[0]
            : null
          : null,
      stocks: group1.length > 0 ? group1[0].listStock : [],
    };
  }

  sortGroup(group, _sort) {
    if (_sort.enabled == true) {
      let sortObject = {
        ["listStock." + _sort.column]: parseInt(_sort.type),
      };

      return group
        .unwind("listStock")
        .sort(sortObject)
        .group({
          _id: "$_id",
          user: { $first: "$user" },
          listStock: { $push: "$listStock" },
        });
    }
    return group;
  }

  pageGroup(group, _page) {
    let size = parseInt(_page.size);
    let page = parseInt(_page.page);
    if (_page.enabled == true) {
      return group
        .unwind("listStock")
        .skip((page - 1) * size)
        .limit(size)
        .group({
          _id: "$_id",
          user: { $first: "$user" },
          listStock: { $push: "$listStock" },
        });
    }
    return group;
  }

  async createGroup(name, _id) {
    let group = await groupModel.findOne({ name, user: _id });
    if (group == null)
      return await groupModel.insertMany([
        Object.assign(
          { _id: (await CurrentId.getId("group")) + 1 },
          { listStock: [], name, user: _id }
        ),
      ]);
    return null;
  }

  async addToGroup(groupId, data, userId) {
    let groups = await groupModel.findOne({ _id: groupId, user: userId });
    let listGroup = groups.listStock;
    let s = new Set(listGroup.concat(data));
    return await groupModel.updateOne(
      { _id: groupId },
      { listStock: Array.from(s) }
    );
  }

  async removeFromGroup(groupId, data, userId) {
    let groups = await groupModel.find({ _id: groupId, user: userId });
    let listGroup = groups[0].listStock;
    let newGroup = listGroup.filter((x) => !data.includes(x));
    return await groupModel.updateOne(
      { _id: groupId },
      { listStock: newGroup }
    );
  }
  async removeGroup(groupId, userId) {
    return await groupModel.remove({ _id: groupId, user: userId });
  }
}

module.exports = new myGroupService();
