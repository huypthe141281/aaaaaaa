const schedule = require("node-schedule");
const moment = require("moment");

const stockService = require("../services/Stock.service");
const jwtHelper = require("../helper/Jwt.helper");
const userOnline = require("../models/UserOnline");
const expectedService = require("../services/Expected.service");
const CONSTANT = require("../helper/Constant.helper");
function changeReport(io) {
  stockService.autoUpdate().then(async (response) => {
    console.log(response.update.length);
    io.emit("autoUpdate", { data: response.update });
    let userOnlines = userOnline.getUser();

    for (let userOnline1 of userOnlines) {
      let listE = await expectedService.getExpectedPrice(
        userOnline1._id,
        { enabled: false },
        { enabled: false },
        { enabled: false }
      );
      let sender = [];
      if (listE.length != 0) {
        let expectedPrice = listE[0].expected;

        for (let ex of expectedPrice) {
          let stock = response.all.find((a) => {
            return a.code == ex.stock.code;
          });

          if (stock != undefined) {
            if (ex.value > stock.tc) {
              sender.push({
                code: ex.stock.code,
                price: stock.tc,
                expected: ex.value,
                time: moment().format("h:mm:ss a"),
              });
            }
          }
        }
        if (sender.length > 0) io.to(userOnline1._id).emit("repne", sender);
      }
    }
  });
}

function autoUpdate(io) {
  const job = schedule.scheduleJob(`*/${CONSTANT.UPDATETIME} * * * * *`, () => {
    changeReport(io);
  });
  io.on("connection", (socket) => {
    console.log(socket.id);
    socket.on("online", (data) => {
      if (data != undefined) {
        userOnline.addUser(jwtHelper.veryfyData(data)._id);
        socket.join(jwtHelper.veryfyData(data)._id);
      }
    });
  });
}
module.exports = { autoUpdate };
