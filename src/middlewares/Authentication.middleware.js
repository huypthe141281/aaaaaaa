const apiResponse = require("../helper/apiResponse.helper");

module.exports = (req, res, next) => {
  if (res.locals.user == null) {
    return apiResponse.unauthorizeResponse(res, "Must login");
  }
  next();
};
