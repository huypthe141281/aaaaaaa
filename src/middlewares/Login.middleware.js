const _ = require("lodash");

const userService = require("../services/User.service");
const jwthelper = require("../helper/Jwt.helper");

module.exports = async (req, res, next) => {
  try {
    let token = req.header("x-access-token");

    if (token == undefined || token == null) {
      next();
      return;
    }
    let _id = jwthelper.veryfyData(token)._id;

    let user = await userService.getUserById(_id);

    if (_.isEmpty(user)) {
      res.locals.user = null;
    } else {
      res.locals.user = user;
      res.locals.user.password = undefined;
    }
    next();
  } catch (error) {
    res.clearCookie("token_id");
    res.redirect("/index");
  }
};
