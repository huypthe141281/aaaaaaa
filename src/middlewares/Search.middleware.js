// const apiResponse = require("../helper/apiResponse.helper");

module.exports = (req, res, next) => {
  res.locals._search = {
    enabled: false,
  };
  if (req.query.hasOwnProperty("_search")) {
    let { searchby, infor } = req.query;
    Object.assign(res.locals._search, {
      enabled: true,
      infor,
      searchby,
    });
  }

  next();
};
