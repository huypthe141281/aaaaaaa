const apiResponse = require("../helper/apiResponse.helper");

module.exports = (req, res, next) => {
  res.locals._sort = {
    enabled: false,
  };
  if (req.query.hasOwnProperty("_sort")) {
    let { column, type } = req.query;
    if (
      (type != -1 && type != 1) ||
      (column == undefined ? "" : column.trim()) == ""
    ) {
      apiResponse.badRequestResponse(res, "bad params value ");
      return;
    }
    Object.assign(res.locals._sort, {
      enabled: true,
      type,
      column,
    });
  }
  next();
};
