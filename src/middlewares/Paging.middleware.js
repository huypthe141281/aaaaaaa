const apiResponse = require("../helper/apiResponse.helper");
module.exports = (req, res, next) => {
  res.locals._page = {
    enabled: false,
  };

  let { page, size } = req.query;

  if (req.query.hasOwnProperty("_page")) {
    if (
      !Number.isInteger(parseInt(page)) ||
      !Number.isInteger(parseInt(size)) ||
      (page == undefined ? "" : page.trim()) == "" ||
      (size == undefined ? "" : size.trim()) == ""
    ) {
      return apiResponse.badRequestResponse(
        res,
        "params page and size must be integer number"
      );
    }
    if (parseInt(page) < 0 || parseInt(size) < 0) {
      return apiResponse.badRequestResponse(
        res,
        "params page and size invalid"
      );
    }
    Object.assign(res.locals._page, {
      enabled: true,
      page: req.query.page,
      size: req.query.size,
    });
  }
  next();
};
