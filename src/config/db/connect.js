const CONSTANT = require("../../helper/Constant.helper");
const mongoose = require("mongoose");
module.exports.connect = async (app) => {
  mongoose.connect(
    CONSTANT.MONGOURI[app.settings.env],
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    },
    function (err, res) {
      if (err) {
        console.log("Error connecting to the database.. " + err);
      } else {
        console.log(
          "Connected to Database: " + CONSTANT.MONGOURI[app.settings.env]
        );
      }
    }
  );
};
