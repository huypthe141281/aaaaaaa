const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const expectedSchema = new Schema({
  _id: { type: Number, require: true },
  user: { type: Number, require: true },
  expected: [
    {
      stock: { type: Number, require: true, ref: "Stock" },
      value: { type: Number, require: true },
    },
  ],
});

module.exports = mongoose.model("Expectedprice", expectedSchema);
