const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const groupSchema = new Schema({
  _id: { type: Number, require: true },
  listStock: [{ type: Number, require: true, ref: "Stock" }],
  name: { type: String, require: true },
  user: { type: Number, require: true },
});

module.exports = mongoose.model("Group", groupSchema);
