const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const StockSchema = new Schema({
  _id: { type: Number, default: 1 },
  code: { type: String, require: true },
  tc: { type: Number, default: 0 },
  ceil: { type: Number, default: 0 },
  floor: { type: Number, default: 0 },
  buyPrice3: { type: Number, default: 0 },
  buyKl3: { type: Number, default: 0 },
  buyPrice2: { type: Number, default: 0 },
  buyKl2: { type: Number, default: 0 },
  buyPrice1: { type: Number, default: 0 },
  buyKl1: { type: Number, default: 0 },
  delta: { type: Number, default: 0 },
  price: { type: Number, default: 0 },
  kl: { type: Number, default: 0 },
  tongkl: { type: Number, default: 0 },
  salePrice1: { type: Number, default: 0 },
  saleKl1: { type: Number, default: 0 },
  salePrice2: { type: Number, default: 0 },
  saleKl2: { type: Number, default: 0 },
  salePrice3: { type: Number, default: 0 },
  saleKl3: { type: Number, default: 0 },
  high: { type: Number, default: 0 },
  low: { type: Number, default: 0 },
});

StockSchema.query.sortable = function (_sort) {
  if (_sort.enabled == true) {
    let sortObj = {
      [_sort.column]: _sort.type,
    };

    return this.sort(sortObj);
  }
  return this;
};

StockSchema.query.pagingable = function (_page) {
  if (_page.enabled == true) {
    return this.skip((_page.page - 1) * _page.size).limit(_page.size * 1);
  }
  return this;
};
module.exports = mongoose.model("Stock", StockSchema);
