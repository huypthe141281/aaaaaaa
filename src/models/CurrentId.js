const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const currentIdSchema = new Schema({
  _id: { type: String, require: true },
  count: { type: Number, require: true },
});

module.exports = mongoose.model("CurrentId", currentIdSchema);
