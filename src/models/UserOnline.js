let UserOnline = [];

function addUser(_id) {
  if (!UserOnline.some((a) => a._id == _id)) UserOnline.push({ _id });
}

function getUser() {
  return UserOnline;
}

module.exports = { addUser, getUser };
