const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userSchema = new Schema({
  _id: { type: Number, require: true },
  email: { type: String, require: true },
  password: { type: String, require: true },
  name: { type: String, require: true },
  active: { type: Boolean, default: false },
  activeCode: {
    type: String,
  },
});

module.exports = mongoose.model("User", userSchema);
