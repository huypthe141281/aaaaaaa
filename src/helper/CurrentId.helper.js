const currentId = require("../models/CurrentId");

module.exports.getId = async (_id) => {
  let id = await currentId.collection.findOneAndUpdate(
    { _id },
    { $inc: { count: 1 } },
    { new: true }
  );
  return id.value.count;
};
