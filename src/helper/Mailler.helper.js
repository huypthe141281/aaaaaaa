const nodemailer = require("nodemailer");

const CONSTANT = require("./Constant.helper");

let { host, port, user, pass } = CONSTANT.EMAILCONFIG;
const transporter = nodemailer.createTransport({
  host,
  port,
  secure: false, // true for 465, false for other ports
  auth: {
    user, // generated ethereal user
    pass, // generated ethereal password
  },
});
module.exports.send = async (to, subject, text, content) => {
  return await transporter.sendMail({
    from: user, // sender address
    to,
    subject,
    text,
    html: `<b>${content}</b>`, // html body
  });
};
