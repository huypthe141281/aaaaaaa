const jwt = require("jsonwebtoken");

const CONSTANT = require("./Constant.helper");

module.exports.signData = (data) => {
  return new Promise((resolve, reject) => {
    jwt.sign(data, CONSTANT.SECRETJWT, (err, token) => {
      if (err) {
        reject(err);
      }
      resolve(token);
    });
  });
};

module.exports.veryfyData = (token) => {
  return jwt.verify(token, CONSTANT.SECRETJWT);
};
