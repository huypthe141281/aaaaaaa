const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

// Router

const stockRouter = require("./src/routers/Stock.router");
const userRouter = require("./src/routers/User.router");
const groupRouter = require("./src/routers/Group.router");
const expectedRouter = require("./src/routers/Expected.router");
// middleware
const sortMiddleware = require("./src/middlewares/Sorting.middleware");
const pageMiddleware = require("./src/middlewares/Paging.middleware");
const loginMiddleware = require("./src/middlewares/Login.middleware");
const authenMiddeware = require("./src/middlewares/Authentication.middleware");
const searchMiddleware = require("./src/middlewares/Search.middleware");
// auto update services
const autoupdateSeriveces = require("./src/services/AutoUpdate.service");
//
const CONSTANT = require("./src/helper/Constant.helper");

const app = express();
const server = require("http").createServer(app);
server.listen(CONSTANT.PORT, () => {
  console.log(`server start at ${CONSTANT.PORT}`);
});

app.use(morgan("combined"));

app.use(cors());

// connect db
const db = require("./src/config/db/connect");
db.connect(app);

//
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  "/stock",
  sortMiddleware,
  pageMiddleware,
  searchMiddleware,
  stockRouter
);
app.use("/user", userRouter);
app.use("/group", loginMiddleware, authenMiddeware, groupRouter);
app.use("/expected", loginMiddleware, authenMiddeware, expectedRouter);

const io = require("socket.io")(server);
autoupdateSeriveces.autoUpdate(io);

module.exports = app;
